#pragma once

#include "bildverwaltung.h"

#include <dlib/dnn.h>
#include <dlib/data_io.h>
#include <iostream>

class NN_Routine final {

public:

    static constexpr unsigned int BILDDIMENSION = Bildverwaltung::GROESSE_MAX;

    static constexpr unsigned int TESTDATEN_MODULO = 4;

    static inline const unsigned int MAX_STEPS = Config::get_ganzzahl<unsigned int>("NN_MAX_STEPS", 100);

    static inline const double AVG_LOSS_LIMIT  = Config::get_gleitkomma<double>("NN_AVG_LOSS_LIMIT", 0.001);

    static inline const double LERNRATE        = Config::get_gleitkomma<double>("NN_LERNRATE", 0.01);

    typedef dlib::matrix<dlib::rgb_pixel> bild_t;

    typedef dlib::loss_multiclass_log<
                dlib::fc<Bildverwaltung::N_BILD_CLUSTER, // Output-Layer
                dlib::relu<dlib::fc<84,
                dlib::relu<dlib::fc<120,
                dlib::max_pool<2,2,2,2,dlib::relu<dlib::con<16,5,5,1,1,
                dlib::max_pool<2,2,2,2,dlib::relu<dlib::con<6,5,5,1,1,
                dlib::input_rgb_image_sized<BILDDIMENSION> // Input-Layer
                >>>>>>>>>>>> netz_t;

    NN_Routine();

    void start();

private:

    bild_t bild_laden(const std::string& pfad) const;

    void bilder_laden();

    void training_durchfuehren();

    void vorhersagen_treffen();

    bool netz_laden();

    bool netz_speichern();

    std::vector<bild_t> bilder_training;
    std::vector<bild_t> bilder_test;
    std::vector<unsigned long> labels_training;
    std::vector<unsigned long> labels_test;
    std::vector<unsigned long> labels_test_wahr;

    netz_t netz;

    /// Ordner, aus dessen Unterverzeichnis die Bilder von dieser Klasse verwendet werden.
    std::string bilderordner = "bilder";

};
