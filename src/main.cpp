#include "bildverwaltung.h"
#include "nn_routine.h"
#include "nn_routine_hogs.h"

int main(int argc, char** argv) {
    std::cout << __func__ << std::endl;

    // Parameter
    if (argc == 2) {

        // NN-Modus starten
        if (strcmp(argv[1], "-nn") == 0) {
            NN_Routine nn;
            nn.start();
        }
        // NN-Hog-Modus starten
        else if (strcmp(argv[1], "-nnhogs") == 0) {
            NN_Routine_Hogs nn;
            nn.start();
        }
        //
        else Bildverwaltung::output_hog(argv[1]);

        #ifdef __linux__
            return 0;
        #else
            return system("pause");
        #endif
    }

    Bildverwaltung bv("bilder");
    bv.clustering_hog();
    bv.ergebnis_hogs_csv_schreiben();
}
