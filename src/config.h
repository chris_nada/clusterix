#pragma once

#include <vector>
#include <map>

/**
 * Verwaltet das Speichern von Werten in einer Konfigurationsdatei.
 * + Das Format der Konfigurationsdatei ist eine unkomprimierte `ini`, mit `=` ohne Leerzeichen / Tabs
 *   zeilenweise getrennter Schlüssel - Wertpaare.
 * + Praktisch kann jede `std::map<std::string, std::string>` gespeichert werde.
 *
 * @author Christian Kriz, modifiziert aus eigenem quelloffenem Projekt:
 *         https://gitlab.com/chris_nada/astro_dm
 * @note Schlüssel müssen einmalig sein.
 * @warning Die Methoden dürfen *nicht* parallel verwendet werden.
 */
class Config final {

public:

    /// Von den Lese/Schreibmethoden verwendeter Pfad zur Konfigurationdatei.
    static inline constexpr const char* PFAD = "config.ini";

    /**
     * Liefert aus der Konfigurationsdatei einen String.
     * @param key Schlüsselwert des Strings.
     * @return String aus der Konfigurationsdatei, der hinter dem Schlüssel steht.
     * Leerer String (`std::string.empty()`), falls nicht gefunden oder leerer Wert.
     */
    static std::string get_string(const std::string& key);

    /**
     * Liefert aus der Konfigurationsdatei eine `bool`. Bools werden als `0` oder `1` in die Datei geschrieben.
     * @param key Schlüsselwert der bool.
     * @return `bool` aus der Konfigurationsdatei, der hinter dem Schlüssel steht.
     * Leerer String (`std::string.empty()`), falls nicht gefunden oder leerer Wert.
     */
    static bool get_bool(const std::string& key, bool standard);

    /**
     * Liefert aus der Konfigurationsdatei eine Gleitkommazahl.
     * @note Intern wird als Cast eine `double` verwendet.
     * @param key Schlüsselwert der Zahl.
     * @return Zu `T` gecastete Zahl aus der Konfigurationsdatei, der hinter dem Schlüssel steht.
     * Liefert `T()`, falls `key` nicht gefunden wird.
     */
    template <typename T, typename = std::enable_if_t<std::is_floating_point<T>::value>>
    static T get_gleitkomma(const std::string& key, T standard = T()) {
        try { return static_cast<T>(std::stod(get_string(key))); }
        catch (std::exception& e) { return standard; }
    }

    /**
     * Liefert aus der Konfigurationsdatei eine Ganzzahl.
     * @note Intern wird als Cast eine `long long` verwendet.
     * @param key Schlüsselwert der Zahl.
     * @return Zu `T` gecastete Zahl aus der Konfigurationsdatei, der hinter dem Schlüssel steht.
     * Liefert `T()`, falls `key` nicht gefunden wird.
     */
    template<typename T, typename = std::enable_if_t<std::is_integral<T>::value>>
    static T get_ganzzahl(const std::string& key, const T standard) {
        try { return static_cast<T>(std::stoll(get_string(key))); }
        catch (std::exception& e) { return standard; }
    }

    /**
     * Speichert gegebene `std::map` als `ini`-Datei ab, aus der mittels der hier gegebenen
     * get-Methoden Werte wieder ausgelesen werden können.
     * @param key_wert_paare Zu speichernde Werte.
     * @note `bool` müssen als `0` oder `1` repräsentiert sein, damit sie korrekt wieder eingelesen werden.
     */
    static void speichern(const std::map<std::string, std::string>& key_wert_paare);

    /**
     * Speichert gegebenes Key-Value paar in der verwendeten Config-Datei ab.
     * @param key Schlüssel (links vom Gleichheitszeichen).
     * @param value Wert (rechts vom Gleichheitszeichen).
     */
    static bool speichern(const std::string& key, const std::string& value);

    /**
     * Liefert einen std::vector aus std::string, die aus einem Text durch einen Separator geteilt wurden.
     * @param text Aufzuteilender Text
     * @param token char, bei dem aufgeteilt wird
     * @return Textteile
     */
    static std::vector<std::string> tokenize(const std::string& text, char token);

};
