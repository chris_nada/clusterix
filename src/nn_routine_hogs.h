#pragma once

#include "bildverwaltung.h"

#include <dlib/dnn.h>

class NN_Routine_Hogs final {

public:

    static constexpr unsigned int TESTDATEN_MODULO = 4;

    static inline const unsigned int MAX_STEPS = Config::get_ganzzahl<unsigned int>("NN_MAX_STEPS_HOGS", 100);

    static inline const double AVG_LOSS_LIMIT  = Config::get_gleitkomma<double>("NN_AVG_LOSS_LIMIT_HOGS", 0.001);

    static inline const double LERNRATE        = Config::get_gleitkomma<double>("NN_LERNRATE_HOGS", 0.01);

    typedef Bildverwaltung::hog_t hog_t;

    typedef dlib::matrix<float> nn_input_t;

    typedef dlib::loss_multiclass_log<
    dlib::fc<Bildverwaltung::N_BILD_CLUSTER, // Output-Layer
            dlib::relu<dlib::fc<static_cast<unsigned long>(80 * 0.33),
            dlib::relu<dlib::fc<static_cast<unsigned long>(80 * 0.5),
            dlib::input<nn_input_t> // Input-Layer
    >>>>>> netz_t;

    NN_Routine_Hogs();

    void start();

private:

    void bilder_laden();

    void training_durchfuehren();

    void vorhersagen_treffen();

    bool netz_laden();

    bool netz_speichern();

    std::vector<Bildverwaltung::Einzelergebnis> ergebnisse;
    std::vector<Bildverwaltung::Einzelergebnis> daten_training;
    std::vector<Bildverwaltung::Einzelergebnis> daten_test;
    std::map<std::string, unsigned long> ordner_labels;
    std::vector<unsigned long> vorhersage_labels;

    netz_t netz;

    Bildverwaltung bv;

    /// Ordner, aus dessen Unterverzeichnis die Bilder von dieser Klasse verwendet werden.
    std::string bilderordner = "bilder";

};
