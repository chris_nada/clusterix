#pragma once

#include "config.h"

#include <dlib/clustering.h>

/**
 * // TODO Beschreibung
 * @tparam daten_t Datentyp der einzelnen Einträge in den Sample-Matrizen.
 * @tparam ZEILEN Anzahl Zeilen in den Sample-Matrizen.
 * @tparam SPALTEN Anzahl Spalten in den Sample-Matrizen.
 * @tparam N_CLUSTER Anzahl der Cluster, auf die aufgeteilt werden soll. Muss kleiner sein als `UINT32_MAX`.
 */
template <typename daten_t, unsigned long ZEILEN, unsigned long SPALTEN>
class Clustering_KKM final {

public:

    /// Datentyp der Samples.
    typedef dlib::matrix<daten_t, ZEILEN, SPALTEN> sample_t;

    /// Datentyp des Kernels.
    typedef dlib::radial_basis_kernel<sample_t> kernel_t;

    /**
     * Konstruktor zum Erstellen eines neu anzulernenden Clusterers.
     * @param gamma gamma-Wert des Kernels.
     * @param praezision Präzisionswert des Kernels.
     * @param stuetzvektoren Anzahl nutzbarer Stützvektoren des Kernels.
     */
    explicit Clustering_KKM(
            unsigned long N_CLUSTER,
            float gamma                  = Config::get_gleitkomma<float>("KMEANS_GAMMA", 0.1),
            float praezision             = Config::get_gleitkomma<float>("KMEANS_PRAEZISION", 0.1),
            unsigned long stuetzvektoren = Config::get_ganzzahl<unsigned long>("KMEANS_STUETZVEKTOREN", 10))
        : kc(dlib::kcentroid<kernel_t>(kernel_t(gamma), praezision, stuetzvektoren)),
          kkmeans(kc), N_CLUSTER(N_CLUSTER) {
        #ifndef NDEBUG
            std::cout << "Clustering_KKM(" << N_CLUSTER << ';' << gamma << ';' << praezision << ';';
            std::cout << stuetzvektoren << ')' << std::endl;
        #endif
        kkmeans.set_number_of_centers(N_CLUSTER);
    }

    /**
     * Führt dem Trainingskorpus ein Sample hinzu.
     * @param sample Dem Trainingskorpus hinzuzufügendes Sample; wird manipuliert, daher als Kopie.
     */
    void add_sample(sample_t sample) { {
        for (auto& wert : sample) if (not std::isfinite(wert)) wert = 0.f; // NANs auf 0 setzen.
        samples.push_back(sample); }
    }

    /// Führt das Training durch. Erst danach können Ergebnisse über `get_cluster` abgefragt werden.
    void train() {
        std::cout << "Clustering_KKM::" << __func__ << "()" << std::endl;
        if (samples.empty()) {
            std::cerr << "\tFehler: Keine Samples hinzugefuegt." << std::endl;
            return;
        }
        dlib::pick_initial_centers(N_CLUSTER, zentren, samples, kkmeans.get_kernel());
        kkmeans.train(samples, zentren);
    }

    /**
     * Gibt für gegebenes Sample den zugehörigen Cluster aus.
     * @param sample Zu untersuchendes Sample.
     * @return Nummer des Clusters als `unsigned int`.
     * @warning Das Sample muss hinzugefügt und Training durchgeführt worden sein.
     */
    unsigned long get_cluster(const sample_t& sample) const {
        if (samples.empty()) std::cerr << "\tKeine Samples hinzugefuegt." << std::endl;
        return kkmeans(sample);
    };

private:

    /// Kernel.
    dlib::kcentroid<kernel_t> kc;

    /// Clustering-Algorithmus.
    dlib::kkmeans<kernel_t> kkmeans;

    /// Speicher für die Samples.
    std::vector<sample_t> samples;

    /// Speicher für die initialen Zentren des Kernels.
    std::vector<sample_t> zentren;

    /// Anzahl Cluster.
    const unsigned long N_CLUSTER;

};