#include "nn_routine_hogs.h"

NN_Routine_Hogs::NN_Routine_Hogs() {
    std::cout << "NN_Routine_Hogs()" << std::endl;
}

void NN_Routine_Hogs::start() {
    std::cout << "NN_Routine_Hogs::" << __func__ << std::endl;
    bilder_laden();
    if (ergebnisse.empty()) { std::cerr << "\tKeine Bilder geladen." << std::endl; return; }
    if (!netz_laden()) {
        training_durchfuehren();
        netz_speichern();
    }
    vorhersagen_treffen();
}

void NN_Routine_Hogs::bilder_laden() {
    std::cout << "NN_Routine_Hogs::" << __func__ << std::endl;

    // HOGs kommen vom Clustering
    bv.clustering_hog();
    ergebnisse = bv.get_ergebnisse();

    // Daten aufteilen in Test & Training
    for (unsigned int i = 0; i < ergebnisse.size(); ++i) {
        if (i % TESTDATEN_MODULO == 0) daten_test.push_back(ergebnisse[i]);
        else daten_training.push_back(ergebnisse[i]);
    }

    // Fehlercheck
    if (daten_training.empty()) std::cerr << "\tTrainingsdaten leer" << std::endl;
    if (daten_test.empty()) std::cerr << "\tTestdaten leer" << std::endl;
}

void NN_Routine_Hogs::training_durchfuehren() {
    std::cout << "NN_Routine_Hogs::" << __func__ << std::endl;

    // Trainer Konfigurieren
    dlib::dnn_trainer<netz_t> trainer(netz);
    trainer.be_verbose();

    trainer.set_learning_rate(LERNRATE);
    trainer.set_min_learning_rate(Config::get_gleitkomma<double>("NN_MIN_LERNRATE_HOGS", 0.00001));
    trainer.set_iterations_without_progress_threshold(Config::get_ganzzahl<unsigned long>("NN_LEERLAUFLIMIT", 10));
    //trainer.set_synchronization_file("nn_hogs_sync_dat", std::chrono::seconds(60));
    if (Config::get_bool("NN_LOW_RAM", false)) dlib::set_dnn_prefer_smallest_algorithms();

    // Trainingsdaten extrahieren
    std::vector<nn_input_t> rohe_trainingsdaten;
    std::vector<uint64_t> rohe_trainigslabels;
    for (const auto& ergebnis : daten_training) {
        rohe_trainingsdaten.emplace_back(ergebnis.hog_matrix);
        if (ordner_labels.count(ergebnis.ordner) == 1) {
            rohe_trainigslabels.push_back(ordner_labels[ergebnis.ordner]);
        }
        else {
            ordner_labels[ergebnis.ordner] = ordner_labels.size();
            rohe_trainigslabels.push_back(ordner_labels[ergebnis.ordner]);
        }
    }

    // Training durchführen
    while (trainer.get_train_one_step_calls() < MAX_STEPS) {
        std::cout << "Steps: " << trainer.get_train_one_step_calls() << '/' << MAX_STEPS << std::endl;
        trainer.train_one_step(rohe_trainingsdaten, rohe_trainigslabels);
        if (trainer.get_average_loss() < AVG_LOSS_LIMIT) break;
    }

    // Abschließen
    trainer.get_net();
    netz_speichern();
}

void NN_Routine_Hogs::vorhersagen_treffen() {
    std::cout << "NN_Routine_Hogs::" << __func__ << std::endl;

    // Ausgabe Datei + Konsole
    std::ofstream out("ergebnis_nn_hogs.txt");
    auto schreib = [&out] (const std::string& text) {
        std::cout << text;
        if (out.good()) out << text;
    };

    // (Test)Klassifikation
    float fertig;
    unsigned int korrekt = 0;

    // Daten laden
    std::vector<nn_input_t> netz_input_nn;
    for (const auto& d : daten_test) netz_input_nn.push_back(d.hog_matrix);
    assert(netz_input_nn.size() == daten_test.size());

    // Vorhersage treffen
    vorhersage_labels = netz(netz_input_nn);
    for (unsigned int i = 0; i < netz_input_nn.size(); ++i) {
        fertig = 100.f * (((float) i + 1.f) / (float) netz_input_nn.size());

        // Vorhersage auswerten
        schreib("(Vorhersage/Wahr) = " + std::to_string(vorhersage_labels[i]) + '/' +
                std::to_string(ordner_labels[daten_test[i].ordner]));
        schreib(" " + std::to_string(i + 1));
        std::cout << '\t' << fertig << "%" << std::endl;
        out << '\n';
        if (vorhersage_labels[i] == ordner_labels[daten_test[i].ordner]) ++korrekt;
    }

    // Ausgabe
    schreib("Hog Cluster      = " + std::to_string(Bildverwaltung::N_HOG_CLUSTER) + '\n');
    schreib("Testdaten Modulo = " + std::to_string(TESTDATEN_MODULO) + '\n');
    schreib("Max Steps        = " + std::to_string(MAX_STEPS) + '\n');
    schreib("Lernrate         = " + std::to_string(LERNRATE) + '\n');
    schreib("Avg. loss-limit  = " + std::to_string(AVG_LOSS_LIMIT) + '\n');
    std::stringstream ss;
    ss << netz << '\n';
    schreib(ss.str());
    const float korrekt_p = (100.f * (float) korrekt) / (float) daten_test.size();
    schreib(std::to_string(korrekt) + '/' + std::to_string(daten_test.size()));
    schreib('=' + std::to_string(korrekt_p) + "% Vorhersagen korrekt\n");
}

bool NN_Routine_Hogs::netz_laden() {
    std::cout << "NN_Routine_Hogs::" << __func__ << std::endl;
    try {
        dlib::deserialize("nn_hogs.dat") >> netz;
        return true;
    }
    catch (std::exception& e) { std::cerr << e.what() << std::endl; }
    return false;
}

bool NN_Routine_Hogs::netz_speichern() {
    std::cout << "NN_Routine_Hogs::" << __func__ << std::endl;
    try {
        netz.clean();
        dlib::serialize("nn_hogs.dat") << netz;
        return true;
    }
    catch (std::exception& e) { std::cout << e.what() << std::endl; }
    return false;
}

