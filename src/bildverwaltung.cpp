#include "bildverwaltung.h"
#include "clustering_kkm.h"
#include "klassifikation.h"

#include <dlib/dir_nav.h>
#include <dlib/image_io.h>
#include <dlib/image_transforms.h>
#include <dlib/gui_widgets.h>

void Bildverwaltung::clustering_hog() {
    std::cout << "Bildverwaltung::" << __func__ << '(' << bilderordner << ')' << std::endl;

    // HOG-Clustering konfigurieren
    Clustering_KKM<float, 31, 1> clustering_hog(Config::get_ganzzahl<unsigned long>("N_HOG_CLUSTER", 20));

    // Training / Clustering-Funktionslambda
    auto bildclustering = [&](Modus modus) {
        std::cout << "\tBildclustering; Modus = ";
        std::cout << (modus == TRAINING ? "Training" : "Clustering") << std::endl;

        // Jeder Ordner
        for (const auto& ordner : dlib::directory(bilderordner).get_dirs()) {

            // Jede Bilddatei 2x (ungespiegelt, gespiegelt)
            for (const auto& datei : ordner.get_files()) {
                for (unsigned short i = 0; i < 2; ++i) {
                    #ifndef NDEBUG
                        std::cout << '\t' << datei.full_name() << ' ' << i << std::endl;
                    #endif

                    // Bild laden; Bei Problemen mit dem nächsten Bild weiter
                    dlib::array2d<dlib::rgb_pixel> img;
                    if (!bild_laden(datei.full_name(), img)) continue;
                    if (i != 0) dlib::flip_image_left_right(img);

                    // Hog extrahieren
                    std::vector<hog_t> hogs;
                    dlib::array2d<hog_t> temp_hogs;
                    dlib::extract_fhog_features(img, temp_hogs);
                    #ifndef NDEBUG
                        std::cout << "\tHOGS Spalten/Zeilen " << temp_hogs.nc() << '/' << temp_hogs.nr();
                        std::cout << ' ' << datei.name() << std::endl;
                    #endif

                    // 2D dlib::array zu 1D std::vector konvertieren
                    hogs.reserve(static_cast<size_t>(temp_hogs.nr() * temp_hogs.nc()));
                    for (long r = 0; r < temp_hogs.nr(); ++r) {
                        for (long c = 0; c < temp_hogs.nc(); ++c) {
                            hogs.push_back(temp_hogs[r][c]);
                        }
                    }

                    // Hogs Clustering hinzufügen ODER HOG-Ergebnis schreiben
                    switch (modus) {
                        case TRAINING:
                            for (const auto& hog : hogs) clustering_hog.add_sample(hog);
                            break;
                        case CLUSTERING: {
                            // Cluster zählen
                            hog_matrix_t hog_matrix;
                            for (auto& wert : hog_matrix) wert = 0.0f;
                            for (const auto& hog : hogs) {
                                const auto cluster = clustering_hog.get_cluster(hog);
                                hog_matrix(cluster) += 1.f;
                            }
                            if (hog_matrix.size() == 0) {
                                std::cerr << "\tHOG-Matrix fuer Datei " << datei.name() << " leer." << std::endl;
                                break;
                            }

                            // Normalisieren (wg. Faktor Bildgröße)
                            if (NORMALISIEREN) {
                                float summe = 0.0f;
                                for (const auto& wert : hog_matrix) summe += wert;
                                for (auto& wert : hog_matrix) wert = 100 * wert / summe;
                            }

                            // In Ergebnismatrix eintragen
                            Einzelergebnis einzelergebnis{
                                    .ordner         = ordner.name(),
                                    .datei          = std::to_string('A' + i) + '_' + datei.name(),
                                    .klassifikation = "",
                                    .hog_matrix     = hog_matrix
                            };
                            einzelergebnisse.push_back(einzelergebnis);
                        } break;

                        case DEBUG:break;
                    }
                }
            }
        }
        if (modus == TRAINING) clustering_hog.train();
    };

    // Bild-Clustering-Funktion ausführen
    bildclustering(TRAINING);
    bildclustering(CLUSTERING);

    // Klassifikation durchführen
    if (KLASSIFIZIEREN) {
        dlib::parallel_for(0, einzelergebnisse.size(), [&](unsigned int i) { // multi-threaded
            Klassifikation<N_HOG_CLUSTER, 1> klassifikation(
                    Config::get_gleitkomma<float>("KLASSIFIKATION_GAMMA", 0.1)
            );

            // Training
            for (unsigned int j = 0; j < einzelergebnisse.size(); ++j) {
                if (i == j) continue; // alle = 'Training' außer 1 Bild, welches 'Test' ist
                const Einzelergebnis& einzelergebnis = einzelergebnisse[j];
                klassifikation.add_sample(einzelergebnis.hog_matrix, einzelergebnis.ordner);
            }
            klassifikation.train();
            einzelergebnisse[i].klassifikation = klassifikation.get_klassenname(einzelergebnisse[i].hog_matrix);
            std::cout << "Klassen " << (i+1) << '/' << einzelergebnisse.size() << " = ";
            std::cout << klassifikation.get_anzahl_klassen() << std::endl;
        });
    }

    // Ergebnisse weiterverarbeiten
    if (Config::get_bool("HOG_ERGEBNIS_CLUSTERN", false)) clustering_hog_ergebnis(einzelergebnisse);
}

void Bildverwaltung::clustering_hog_ergebnis(const std::vector<Einzelergebnis>& hog_ergebnisse) {
    std::cout << "Bildverwaltung::" << __func__ << '(' << hog_ergebnisse.size() << ')' << std::endl;

    // K-Means-Wrapper initialisieren
    Clustering_KKM<float, N_HOG_CLUSTER, 1> kmeans(Config::get_ganzzahl<unsigned long>("N_BILD_CLUSTER", 10));

    // Training durchführen
    for (const auto& einzelergebnis : hog_ergebnisse) kmeans.add_sample(einzelergebnis.hog_matrix);
    kmeans.train();

    // Cluster-Ergebnisse sammeln
    std::map<Einzelergebnis, unsigned long> bilder_cluster;
    for (const auto& einzelergebnis : hog_ergebnisse) {
        bilder_cluster[einzelergebnis] = kmeans.get_cluster(einzelergebnis.hog_matrix);
    }

    // Ordner für Cluster erstellen
    for (unsigned int i = 0; i < N_BILD_CLUSTER; ++i) {
        try { dlib::create_directory(std::to_string(i)); }
        catch (std::exception& e) { std::cerr << e.what() << std::endl; }
    }
}

void Bildverwaltung::output_hog(const std::string& datei) {
    std::cout << "Bildverwaltung::" << __func__ << '(' << datei << ')' << std::endl;

    // Bild laden
    dlib::array2d<dlib::rgb_pixel> img;
    bild_laden(datei, img);

    // Bild Pre-HOG-Extraktion speichern
    dlib::save_png(img, "ergebnis.png");

    // HOG-Extraktion + speichern
    dlib::array2d<hog_t> temp_hogs;
    dlib::extract_fhog_features(img, temp_hogs);
    dlib::save_png(dlib::draw_fhog(temp_hogs), "ergebnis_hogs.png");

    // In CSV schreiben
    if (std::ofstream out("ergebnis_hogs.csv"); out.good()) {
        for (decltype(temp_hogs.nr()) r = 0; r < temp_hogs.nr(); ++r) {
            for (decltype(temp_hogs.nr()) c = 0; c < temp_hogs.nc(); ++c) {
                for (auto wert : temp_hogs[r][c]) out << wert << ';';
                out << '\n';
            }
        }
    }
}

void Bildverwaltung::verkleinern(dlib::array2d<dlib::rgb_pixel>& img, unsigned int max) {
    if (img.nc() <= (long) max && img.nr() <= (long) max) return; // Bild überschreitet nicht max

    // Verkleinerung berechnen
    dlib::array2d<dlib::rgb_pixel> temp_img;
    double faktor;
    if (img.nr() > img.nc()) faktor = (double) max / (double) img.nr();
    else faktor = (double) max / (double) img.nc();
    dlib::resize_image(faktor, img);
}

bool Bildverwaltung::bild_laden(const std::string& dateiname, dlib::array2d<dlib::rgb_pixel>& img) {
    // Bild laden, return false bei Fehlschlag
    try { dlib::load_image(img, dateiname); }
    catch (std::exception& e) {
        std::cerr << "\tFehler beim Laden des Bildes " << dateiname << ' ';
        std::cerr << e.what() << std::endl;
        return false;
    }

    // Größe anpassen
    if (GROESSE_ANPASSEN) verkleinern(img, GROESSE_MAX);

    // Sobel Edge-Detection anwenden?
    if (SOBEL_EDGE) {
        dlib::array2d<short> horz_gradient, vert_gradient;
        dlib::sobel_edge_detector(img, horz_gradient, vert_gradient);
        dlib::suppress_non_maximum_edges(horz_gradient, vert_gradient, img);
    }
    return true; // Bild erfolgreich geladen + bearbeitet
}

void Bildverwaltung::ergebnis_hogs_csv_schreiben() {
    // Clustering-Ergebnisse + Klassifikationergebnisse in Datei schreiben
    if (std::ofstream out("einzelcluster_hog.csv"); out.good()) {

        // Überschriften schreiben
        out << "Ordner;Datei;";
        if (KLASSIFIZIEREN) out << "Klasse;";
        for (unsigned int i = 0; i < N_HOG_CLUSTER; ++i) out << ";c" << i;
        out << '\n';

        // Ergebnisse schreiben ';' - getrennt
        for (const auto& ergebnis : einzelergebnisse) out << ergebnis << '\n';
    } else std::cerr << "\teinzelcluster_hog.csv konnte nicht geschrieben werden." << std::endl;
}
