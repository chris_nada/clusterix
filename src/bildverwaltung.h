#pragma once

#include "config.h"

#include <utility>
#include <dlib/matrix.h>
#include <map>
#include <string>
#include <ostream>

/**
 * TODO Beschreibung
 */
class Bildverwaltung final {

public:

    /// Standardkonstruktor. Es werden die Bilder in den Unterverzeichnissen von `ordner` verwendet.
    explicit Bildverwaltung(const std::string& ordner = "bilder") : bilderordner(ordner) {}

    /// Datentyp einer einzelnen HOG-Matrix.
    typedef dlib::matrix<float, 31, 1> hog_t;

    /// Bestimmt, in wieviele Cluster HOGs aufgeteilt werden.
    static inline const unsigned int N_HOG_CLUSTER  = 10;

    /// Bestimmt, in wieviele Cluster die gezählten + geclusterten HOG-Summen aufgeteilt werden.
    static inline const unsigned int N_BILD_CLUSTER = 5;

    /// Maximale Größe, die Bilder haben dürfen. Zu große Bilder werden auf Maximalgröße verkleinert.
    static inline const unsigned int GROESSE_MAX = 200;

    /// Wenn `true`, werden alle HOG-Cluster-Anteile relativiert in Prozent angegeben.
    static inline const bool NORMALISIEREN = Config::get_bool("NORMALISIEREN", true);

    /// Wendet die Sobel-Kantenerkennung vor der HOG-Extraktion an.
    static inline const bool SOBEL_EDGE = Config::get_bool("SOBEL_EDGE", true);

    /// Bilder mit Breite oder Höhe größer als `GROESSE_MAX` werden automatisch verkleinert.
    static inline const bool GROESSE_ANPASSEN = Config::get_bool("VERKLEINERN", true);

    static inline const bool KLASSIFIZIEREN = Config::get_bool("KLASSIFIZIEREN", false);

    /// Erzeugt zur Veranschaulichung Bilder und eine CSV-Datei der Zwischenergebnisse.
    static void output_hog(const std::string& datei);

    /// Lädt ein Bild aus gegebener Datei. Gibt bei Fehlschlag `false` zurück.
    static bool bild_laden(const std::string& dateiname, dlib::array2d<dlib::rgb_pixel>& img);

    void ergebnis_hogs_csv_schreiben();

    /// Hauptfunktion: Wendet das Clustering auf die HOG-Merkmale an.
    void clustering_hog();


    /// Datentyp, in den die geclusterten HOG-Ergebnisse geschrieben werden.
    typedef dlib::matrix<float, N_HOG_CLUSTER, 1> hog_matrix_t;

    /**
     * Speicher für die Cluster der extrahierten HOGs aus einem einzelnen Bild.
     */
    typedef struct Einzelergebnis {
        std::string ordner;
        std::string datei;
        std::string klassifikation;
        hog_matrix_t hog_matrix;

        /// Operatoren sind nötig bspw. zum sortieren in `std::map`s.
        bool operator<(const Einzelergebnis& rhs) const {
            if (ordner != rhs.ordner) return ordner < rhs.ordner;
            return datei < rhs.datei;
        }
        bool operator>(const Einzelergebnis& rhs) const { return rhs < *this; }
        bool operator<=(const Einzelergebnis& rhs) const { return !(rhs < *this); }
        bool operator>=(const Einzelergebnis& rhs) const { return !(*this < rhs); }
        bool operator==(const Einzelergebnis& rhs) const { return ordner == rhs.ordner && datei == rhs.datei; }
        bool operator!=(const Einzelergebnis& rhs) const { return !(rhs == *this); }

        /// Schreibt Ergebnis als CSV
        friend std::ostream& operator<<(std::ostream& os, const Einzelergebnis& ergebnis) {
            os << ergebnis.ordner << ';'; // Ordnername
            os << ergebnis.datei  << ';'; // Dateiname
            if (KLASSIFIZIEREN) os << ergebnis.klassifikation << ';'; // Klassifikation
            for (const auto& wert : ergebnis.hog_matrix) os << ';' << wert; // Werte eintragen
            return os;
        }
    } Einzelergebnis;

    /// Getter für alle Einzelergebnisse.
    const std::vector<Einzelergebnis>& get_ergebnisse() const { return einzelergebnisse; }

private:

    /// Modus, in dem sich die Klasse momentan befindet.
    typedef enum Modus { TRAINING, CLUSTERING, DEBUG } Modus;

    /// Verkleinert ein Bild auf `max`-Größe, falls es diese überschreitet.
    static void verkleinern(dlib::array2d<dlib::rgb_pixel>& img, unsigned int max);

    /// Wendet auf das vorangegangene Clustern der HOG-Features ein weiteres Clustering an.
    void clustering_hog_ergebnis(const std::vector<Einzelergebnis>& hog_ergebnisse);

    // Ergebnisspeicher: Key = Datei, Value = HOG-Matrix
    std::vector<Einzelergebnis> einzelergebnisse;

    /// Ordner, aus dessen Unterverzeichnis die Bilder von dieser Klasse verwendet werden.
    std::string bilderordner;

};
