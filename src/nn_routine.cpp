#include <dlib/gui_widgets.h>
#include "nn_routine.h"
#include "bildverwaltung.h"

NN_Routine::NN_Routine() {
    std::cout << "NN_Routine()" << std::endl;
}

void NN_Routine::start() {
    std::cout << "NN_Routine::" << __func__ << std::endl;
    bilder_laden();
    if (bilder_training.empty()) { std::cerr << "\tKeine Bilder geladen." << std::endl; return; }
    if (!netz_laden()) {
        training_durchfuehren();
        netz_speichern();
    }
    vorhersagen_treffen();
}

void NN_Routine::bilder_laden() {
    std::cout << "\tNN_Routine::" << __func__ << std::endl;

    // Modus A: Alle Unterordner nach Bildern durchsuchen
    auto alle_unterordner = dlib::directory(bilderordner).get_dirs();
    if (Config::get_bool("NN_BINARY", false)) alle_unterordner = {
            dlib::directory(Config::get_string("NN_FOLDER0")),
            dlib::directory(Config::get_string("NN_FOLDER1"))
    };
    for (unsigned int i = 0; i < alle_unterordner.size(); ++i) {
        const auto& unterordner = alle_unterordner[i];

        // Jede Datei prüfen
        const auto& dateien = unterordner.get_files();
        for (unsigned int k = 0; k < dateien.size(); ++k) {
            const auto& datei = dateien[k];

            #ifndef NDEBUG
                std::cout << "\tWird geladen: " << datei.full_name() << std::endl;
            #endif

            // Bild laden + hinzufügen; Jedes x.te Bild zur Verifikation
            try {
                auto bild = bild_laden(datei.full_name());
                auto bild_gespiegelt = bild; // gespiegelte Kopie nutzen
                dlib::flip_image_left_right(bild_gespiegelt);

                switch (k % TESTDATEN_MODULO) {
                    case 0:
                        bilder_test.push_back(bild);
                        labels_test_wahr.push_back(i);
                        bilder_test.push_back(bild_gespiegelt);
                        labels_test_wahr.push_back(i);
                        break;
                    default:
                        bilder_training.push_back(bild);
                        labels_training.push_back(i);
                        bilder_training.push_back(bild_gespiegelt);
                        labels_training.push_back(i);
                        break;
                }

            } catch (std::exception& e) { /* Datei ist möglicherweise kein Bild */ }
        }
    }
}

void NN_Routine::training_durchfuehren() {
    std::cout << "\tNN_Routine::" << __func__ << std::endl;

    // Trainer Konfigurieren
    dlib::dnn_trainer<netz_t> trainer(netz);
    trainer.be_verbose();

    trainer.set_learning_rate(LERNRATE);
    trainer.set_min_learning_rate(Config::get_gleitkomma<double>("NN_MIN_LERNRATE", 0.00001));
    trainer.set_iterations_without_progress_threshold(Config::get_ganzzahl<unsigned long>("NN_LEERLAUFLIMIT", 10));
    trainer.set_synchronization_file("nn_sync_dat", std::chrono::minutes(5));
    if (Config::get_bool("NN_LOW_RAM", false)) dlib::set_dnn_prefer_smallest_algorithms();

    // Training durchführen
    while (trainer.get_train_one_step_calls() < MAX_STEPS) {
        std::cout << "Steps: " << trainer.get_train_one_step_calls() << '/' << MAX_STEPS << std::endl;
        trainer.train_one_step(bilder_training, labels_training);
        if (trainer.get_average_loss() < AVG_LOSS_LIMIT) break; // genug (?)
    }

    // Abschließen
    trainer.get_net();
    netz_speichern();
}

void NN_Routine::vorhersagen_treffen() {
    std::cout << "\tNN_Routine::" << __func__ << std::endl;

    // Ausgabe Datei + Konsole
    std::ofstream out("ergebnis_nn_direkt.txt");
    auto schreib = [&out] (const std::string& text) {
        std::cout << text;
        if (out.good()) out << text;
    };

    float fertig;
    if (!Config::get_bool("NN_BINARY", false)) {

        // Reguläre Klassifikation
        unsigned int korrekt = 0;
        labels_test = netz(bilder_test);
        for (unsigned int i = 0; i < labels_test.size(); ++i) {
            fertig = 100.f * (((float) i + 1.f) / (float) labels_test.size());
            schreib("(Vorhersage/Wahr) = " + std::to_string(labels_test[i]) + '/' + std::to_string(labels_test_wahr[i]));
            schreib(" " + std::to_string(i + 1));
            std::cout << '\t' << fertig << "%" << std::endl;
            out << '\n';
            if (labels_test[i] == labels_test_wahr[i]) ++korrekt;
        }

        // Ausgabe
        schreib("Bilddimension    = " + std::to_string(BILDDIMENSION) + '\n');
        schreib("Testdaten Modulo = " + std::to_string(TESTDATEN_MODULO) + '\n');
        schreib("Max Steps        = " + std::to_string(MAX_STEPS) + '\n');
        schreib("Lernrate         = " + std::to_string(LERNRATE) + '\n');
        schreib("Avg. loss-limit  = " + std::to_string(AVG_LOSS_LIMIT) + '\n');
        std::stringstream ss;
        ss << netz << '\n';
        schreib(ss.str());
        const float korrekt_p = (100.f * (float) korrekt) / (float) labels_test.size();
        schreib(std::to_string(korrekt) + '/' + std::to_string(labels_test.size()));
        schreib('=' + std::to_string(korrekt_p) + "% Vorhersagen korrekt\n");
    }
    else {
        // Binäre Klassifikation [nur zu Testzwecken genutzt]
        dlib::directory test_dir(Config::get_string("NN_TEST"));
        for (const auto& datei : test_dir.get_files()) {
            try {
                auto bild = bild_laden(datei.full_name());
                auto label = netz(bild);
                std::cout << label << " " << datei.full_name() << std::endl;
                dlib::image_window fenster;
                fenster.set_image(bild);
                fenster.show();
                while (!fenster.is_closed()) { }
            } catch(std::exception& e) { /* Datei ungültig / kein Bild */ }
        }
    }
}

bool NN_Routine::netz_laden() {
    std::cout << "\tNN_Routine::" << __func__ << std::endl;
    try {
        dlib::deserialize("nn.dat") >> netz;
        return true;
    }
    catch (std::exception& e) { std::cerr << e.what() << std::endl; }
    return false;
}

bool NN_Routine::netz_speichern() {
    std::cout << "\tNN_Routine::" << __func__ << std::endl;
    try {
        netz.clean();
        dlib::serialize("nn.dat") << netz;
        return true;
    }
    catch (std::exception& e) { std::cout << e.what() << std::endl; }
    return false;
}

NN_Routine::bild_t NN_Routine::bild_laden(const std::string& pfad) const {
    bild_t bild_konvertiert(BILDDIMENSION, BILDDIMENSION);
    dlib::array2d<dlib::rgb_pixel> img(BILDDIMENSION, BILDDIMENSION);
    dlib::array2d<dlib::rgb_pixel> img_temp;
    if (!Bildverwaltung::bild_laden(pfad, img_temp)) {
        throw std::runtime_error("Bild konnte nicht geladen werden " + pfad);
    }
    dlib::resize_image(img_temp, img);

    // Konvertieren
    for (decltype(img.nr()) r = 0; r < img.nr(); ++r) {
        for (decltype(img.nc()) c = 0; c < img.nc(); ++c) {
            bild_konvertiert(r, c) = img[r][c];
        }
    }
    return bild_konvertiert;
}
