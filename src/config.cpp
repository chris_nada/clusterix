#include "config.h"

#include <fstream>
#include <vector>
#include <iostream>
#include <sstream>

std::vector<std::string> Config::tokenize(const std::string& text, const char token) {
    std::vector<std::string> teile;
    unsigned long long anfang = 0;
    unsigned long long ende = 0;
    while ((ende = text.find(token, anfang)) != std::string::npos) {
        teile.push_back(text.substr(anfang, ende - anfang));
        anfang = ende;
        anfang++;
    }
    teile.push_back(text.substr(anfang));
    return teile;
}

std::string Config::get_string(const std::string& key) {
    std::ifstream in(PFAD);
    if (in.good()) {

        // Zeilen durchgehen + am '=' teilen
        for (std::string zeile; std::getline(in, zeile);) {
            const std::vector<std::string>& tokens = tokenize(zeile, '=');

            // key gefunden?
            if (tokens.size() == 2 && tokens[0] == key) {
                #ifndef NDEBUG
                    std::cout << key << '=' << tokens[1] << '\n';
                #endif
                return tokens[1];
            }
        }
    }
    return "";
}

bool Config::get_bool(const std::string& key, const bool standard) {
    const std::string& wert = get_string(key);
    if (!wert.empty()) return wert[0] != '0';
    return standard;
}

void Config::speichern(const std::map<std::string, std::string>& key_wert_paare) {
    for (const auto& paar : key_wert_paare) speichern(paar.first, paar.second);
}

bool Config::speichern(const std::string& key, const std::string& value) {
    bool key_gefunden = false; // Key bereits vorhanden?
    std::stringstream inhalt;  // Zu schreibender Inhalt

    // Bestehende Datei einlesen
    if (std::ifstream in(PFAD); in.good()) {
        for (std::string zeile; std::getline(in, zeile);) {
            if (!key_gefunden && zeile.find(key) != std::string::npos) {
                zeile = (key + "=" + value);
                key_gefunden = true;
            }
            inhalt << zeile << '\n';
        }
    }
    if (!key_gefunden) inhalt << key << '=' << value << '\n';

    // Datei schreiben
    if (std::ofstream out(PFAD); out.good()) {
        for (std::string zeile; std::getline(inhalt, zeile);) out << zeile << '\n';
        return true;
    }
    return false;
}
