#pragma once

#include <dlib/svm.h>

template <unsigned long ZEILEN, unsigned long SPALTEN>
class Klassifikation final {

public:

    /// Datentyp der Samples.
    typedef dlib::matrix<float, ZEILEN, SPALTEN> sample_t;

    /// Kerneltyp des Klassifikators.
    typedef dlib::histogram_intersection_kernel<sample_t> kernel_t;

    /// Typ der Entscheidungsfunktion.
    typedef dlib::decision_function<kernel_t> decision_fun_t;

    /// Konstruktor, der gamma des Kernels festlegt.
    explicit Klassifikation(float gamma) : GAMMA(gamma) {}

    /**
     * Führt dem Trainingskorpus ein Sample hinzu.
     * @param sample Dem Trainingskorpus hinzuzufügendes Sample; wird manipuliert, daher als Kopie.
     */
    void add_sample(sample_t sample, const std::string& klasse) {
        for (auto& wert : sample) if (not std::isfinite(wert)) wert = 0.f; // NANs auf 0 setzen.
        samples.push_back(sample);
        if (klassen_namen.count(klasse) == 0) klassen_namen[klasse] = klassen_namen.size();
        klassen.push_back(klassen_namen[klasse]);
    }

    /// Führt das Training durch. Erst danach können Ergebnisse über `get_klasse` abgefragt werden.
    void train() {
        std::cout << "Klassifikation::" << __func__ << "()" << std::endl;
        #ifndef NDEBUG
            trainer.be_verbose();
        #endif
        trainer.use_classification_loss_for_loo_cv();
        trainer.set_kernel(kernel_t()); // TODO gamma
        entscheidungsfunktion = trainer.train(samples, klassen);
    }

    /// Liefert die vorhergesagte Klasse für gegebenes Sample.
    unsigned long get_klasse(sample_t sample) const {
        for (auto& wert : sample) if (not std::isfinite(wert)) wert = 0.f; // NANs auf 0 setzen.
        return static_cast<unsigned long>(std::round(entscheidungsfunktion(sample)));
    }

    /// Liefert die vorhergesagte Klasse für gegebenes Sample als String.
    std::string get_klassenname(sample_t sample) const {
        for (auto& wert : sample) if (not std::isfinite(wert)) wert = 0.f; // NANs auf 0 setzen.
        const unsigned long klasse = get_klasse(sample);
        for (const auto& paar : klassen_namen) if (paar.second  == klasse) return paar.first;
        return "";
    }

    /// Liefert, wieviele Klassen verwendet werden.
    unsigned long get_anzahl_klassen() const { return klassen_namen.size(); }

private:

    /// Übersetzt Klassennummer in Klassenbezeichnung
    std::map<std::string, unsigned long> klassen_namen;

    /// Speicher für die Samples.
    std::vector<sample_t> samples;

    /// Speicher für die Klassen der Samples.
    std::vector<float> klassen;

    /// Trainerobjekt.
    dlib::krr_trainer<kernel_t> trainer;

    /// Entscheidungsfunktion.
    decision_fun_t entscheidungsfunktion;

    /// Gamma.
    const float GAMMA;

};