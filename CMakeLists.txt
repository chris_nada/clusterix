cmake_minimum_required(VERSION 3.10)
project(img_clusterer)
set(CMAKE_CXX_STANDARD 17)

# DLib unter Linux kompilieren
if (UNIX)
    add_subdirectory(../dlib dlib_build)
endif()

# Kompiliereinstellungen
if (CMAKE_BUILD_TYPE STREQUAL "Release")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -s -m64 -fopenmp -march=native")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -Wall -Wextra -Werror -Wno-unused-parameter") # debug
endif()

# Quelltextdateien
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/build/)
add_executable(img_clusterer
        src/main.cpp
        src/clustering_kkm.h
        src/bildverwaltung.h src/bildverwaltung.cpp
        src/config.h src/config.cpp
        src/klassifikation.h
        src/nn_routine.h src/nn_routine.cpp
        src/nn_routine_hogs.h src/nn_routine_hogs.cpp)

# Bibliotheken linken
if (WIN32)
    find_package(DLIB REQUIRED)
    find_package(BLAS REQUIRED)
    include_directories(SYSTEM ${DLIB_INCLUDE_DIR})
    target_link_libraries(img_clusterer dlib ${DLIB_LIBRARIES} ${BLAS_LIBRARIES} gif)
    # DLLs in Build-Verzeichnis kopieren
    configure_file(${CMAKE_ROOT}/../../bin/libblas.dll           ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libcblas.dll          ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libdlib.dll           ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libgcc_s_seh-1.dll    ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libgfortran-5.dll     ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libgif-7.dll          ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libjpeg-8.dll         ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/liblapack.dll         ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libopenblas.dll       ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libpng16-16.dll       ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libstdc++-6.dll       ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libwinpthread-1.dll   ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/libquadmath-0.dll     ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
    configure_file(${CMAKE_ROOT}/../../bin/zlib1.dll             ${EXECUTABLE_OUTPUT_PATH} COPYONLY)
else()
    target_link_libraries(img_clusterer dlib::dlib)
endif()


