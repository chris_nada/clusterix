# Einstellungen #######
draw_pie_chars <- 0
draw_boxplots  <- 0
#######################

# Bibliotheken
library(ggplot2)

# Arbeitsverzeichnis = Skriptverzeichnis
this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

# CSV Einlesen
cpp_cluster <- read.csv(file = "_10_200/einzelcluster_hog.csv", header=TRUE, sep=";")
cpp_cluster <- cpp_cluster[-4]

# Cluster-Daten visualisieren
if (draw_boxplots) { # mit 0/1 an/ausschalten
  for (i in 4:ncol(cpp_cluster)) {
    t_col <- colnames(cpp_cluster)[i] # Spaltenname
    plot <- ggplot(data = cpp_cluster) + 
      stat_boxplot(mapping = aes(x = Ordner, y = cpp_cluster[, c(t_col)])) +
      labs(y = t_col) # Label korrigieren
    ggsave(filename = paste(t_col, ".png", sep = ""), plot = plot)
  }
}

# Mittelwerte bestimmen
cluster_means <- aggregate(cpp_cluster[,4:ncol(cpp_cluster)], list(cpp_cluster$Ordner), mean)

# Pie-Charts generieren
if (draw_pie_chars) { # mit 0/1 an/ausschalten
  for (i in 1:5) {
    means_einzeln <- cluster_means[i,, drop = FALSE]
    means_einzeln <- as.numeric(as.vector(t(means_einzeln)[-c(1)]))
    png(filename = paste(cluster_means[i, 1], ".png", sep = ""))
    pie(x = means_einzeln, main = cluster_means[i, 1])
    dev.off()
  }
}

# Clustering der relativen HOG-Häufigkeiten
cpp_angepasst <- as.data.frame(cpp_cluster)
cluster_data  <- cpp_angepasst[, 4:ncol(cpp_angepasst)]
r_cluster_kmeans <- kmeans(x = cluster_data, centers = 5) # 5 Cluster
r_cluster_hier   <- hclust(d = dist(cluster_data))
cpp_angepasst$r_cluster_k <- r_cluster_kmeans$cluster
cpp_angepasst$r_cluster_h <- cutree(r_cluster_hier, k = 5)

# Klassifikation der relativen HOG-Häufigkeiten


