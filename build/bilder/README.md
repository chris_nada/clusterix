Bilder in den hier enthaltenden Unterverzeichnissen wurden vom Webcrawler automatisch heruntergeladen und
nachträglich manuell gefiltert; sie unterliegen dem Urheberrecht der jeweiligen Verfasser.
Über die sogenannte reverse-image-search (auf yandex, google...) lassen sich die jeweiligen Urheber ermitteln.
Die Bilder werden in diesem Projekt zu rein wissenschaftlichen Zwecken verwendet.
