# -*- coding: utf-8 -*-
import socks, socket
import urllib, urllib2
import os, sys
from PIL import Image
from os.path import isfile, join
from multiprocessing.dummy import Pool as ThreadPool 
from StringIO import StringIO

### Konfiguration
search   = "https://yandex.ru/images/search?text="
suffix   = "&rpt=image&p="
words    = "church+front"
# In diesen Ordner werden die Bilder geschrieben
folder   = os.path.dirname(os.path.realpath(sys.argv[0])) + '/' + words.replace('+','_')
# Wieviele Ergebnisseiten crawlen (von Seite [x] bis Seite [y])
pages    = range(1,2)
# Ordner auf vorhandene Bilder überprüfen
precheck = False

# Speicher für vorhandene RGB-Durchschnittswerte zum Bildervergleich
knownrgb = []
# Zu verwendende Threads beim Download
pool = ThreadPool(4)
# Bei Bann durch Bot-Schutz einfach Proxy wechseln
socks.set_default_proxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket
### Konfiguration Ende

def download(image):
    print "download: ", image
    global folder
    try:
        if not os.path.exists(folder):
            os.makedirs(folder)
        filename = folder + image[image.rindex('/'):len(image)]
        if not os.path.isfile(filename):
            image = urllib.urlopen(image).read()
            if sys.getsizeof(image) > 40000: # /1000 = KB # Mindestgröße (Thumbnails unbrauchbar)
                f = open(filename, "wb")
                f.write(image)
                f.close()
                rgb(filename) # check durchführen
    except Exception as e:
        print e.message

# Durchsucht HTML-Quelltext nach Bildern. 
# Bilder in gegebenen Formaten, in 'exts' definiert,
# werden heruntergeladen.
def crawl_page():
    print "crawl_page"
    try:
        f = open('result.txt', 'r')
        response = f.read()
        f.close()
        response = response.split('"')
        threads = []
        
        exts = [".jpg", ".png"]

        for identifier in exts:
            for part in response:
                if "http" in part and identifier in part: 
                    part = part[part.index("http"):part.index(identifier)+len(identifier)]
                    part = part.replace("%3A",":")
                    part = part.replace("%2F","/")
                    print part, "\n"
                    threads.append(part)

        pool.map(download, threads)
        
    except Exception as e:
        print e

# Startet eine neue Suche und schreibt 
# die Antwort (HTML-Quelltext) in eine Textdatei
def searchnew(page):
    opener = urllib2.build_opener()
    opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.3) Gecko/20150323 Firefox/31.9 PaleMoon/25.3.1')]
    response = opener.open(search + words + suffix + str(page))
    response = response.read()
    f2 = open('result.txt', 'w')
    f2.write(response)
    f2.close()

# Bildet grob einen rgb-Durchschnittswert eines Bildes, der
# vor dem Download mit allen bestehenden Bildern verglichen wrid, 
# um vorzubeugen, dass gleiche Bilder (verschiedener Größe)
# mehrfach heruntergeladen werden.
def rgb(pic):
    print "rgb:", pic
    try:
        global knownrgb
        img = Image.open(pic)
        pixels = img.load()
        #print img.size # Bildgröße
        r = 0
        g = 0
        b = 0
        for x in range(0, img.size[0]):
            for y in range(0, img.size[1]):
                r += pixels[x,y][0]
                g += pixels[x,y][1]
                b += pixels[x,y][2]
        r /= img.size[0] * img.size[1]
        g /= img.size[0] * img.size[1]
        b /= img.size[0] * img.size[1]
        match = False
        for rgb in knownrgb:
            diff = [abs(rgb[0]-r), abs(rgb[1]-g), abs(rgb[2]-b)]
            if diff[0] < 10 and diff[1] < 10 and diff[2] < 10:
                print "\tmatch", diff[0], diff[1], diff[2]
                os.remove(pic)
                return True
        if not match:
            print "\tno match"
            knownrgb.append([r,g,b])
            return False       
    except Exception as e:
        print e
        return False
    return False    

def rgb_file(path):
    print "rgb_file", path

# Bereits vorhandene überprüfen
if precheck:
    saved = [f for f in os.listdir(folder) if isfile(join(folder, f))]
    saved = [folder + "/" + f for f in saved]
    pool.map(rgb, saved)
    
# Crawl/Download
for page in pages:
    searchnew(page)
    crawl_page()
    pass
